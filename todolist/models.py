from django.db import models
from django.utils import timezone


class Task(models.Model):
    id = models.AutoField('Task id', primary_key=True)
    creator_id = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    parent_id = models.ForeignKey('self', on_delete=models.CASCADE, null=True,
                                  blank=True)
    project_id = models.ForeignKey('Project', on_delete=models.CASCADE,
                                   null=True, blank=True)
    child_order = models.IntegerField('Place order as subtask', default=1)
    created = models.DateTimeField('Creation time', default=timezone.now())
    changed = models.DateTimeField('Last change time', default=timezone.now())

    title = models.CharField('Task title', max_length=300)
    description = models.TextField('Task description', null=True, blank=True)

    due = models.DateTimeField('Task due date', null=True, blank=True)
    completed = models.BooleanField('Is task completed', default=False)

    priority = models.IntegerField("Task's priority", default=1)

    def __str__(self):
        return f'{self.title} by {self.creator_id.username}'


class Project(models.Model):
    id = models.AutoField('Project id', primary_key=True)
    creator_id = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    parent_id = models.ForeignKey('self', on_delete=models.CASCADE, null=True,
                                  blank=True)
    child_order = models.IntegerField('Place order as subproject', default=1)
    title = models.CharField('Project title', max_length=300)
    description = models.TextField('Project description', null=True, blank=True)
    mark = models.IntegerField('Project mark', default=1)

    def __str__(self):
        return f'{self.title}'

