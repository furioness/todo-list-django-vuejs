export {deleteNode, getNode, flatten, treeify, setNodeProps, getFormattedDateNow}

import dateformat from "dateformat"
import clonedeep from 'lodash.clonedeep'
import Vue from 'vue'




function getFormattedDateNow(){
  return dateformat(Date.now(), 'yyyy-mm-d HH:MM:ss')
}

function deleteNode(nodeList, id) {
  for (const [idx, node] of nodeList.entries()) {
    console.log(node, idx)
    if (node.id === id) {
      nodeList.splice(idx, 1)
      return true
    }
    if (node.children.length != 0) {
      deleteNode(node.children, id)
    }
  }
  return false
}

function getNode(nodeList, id) {
  let stack = [...nodeList]
  let node
  while (stack.length > 0) {
    node = stack.pop()
    if (!node)
    //return null
      throw `Node with ${id} isn't found`

    if (node.id === id) {
      return node
    } else {
      stack = stack.concat(node.children)
    }
  }
}

function setNodeProps(node, props) {
  for(const [key, value] of Object.entries(props)) {
    Vue.set(node, key, value)
  }
}

function treeify(list, idAttr, parentAttr, childrenAttr) {
  // from https://stackoverflow.com/questions/22367711/construct-hierarchy-tree-from-flat-list-with-parent-field/22367819#22367819
  if (!idAttr) idAttr = 'id';
  if (!parentAttr) parentAttr = 'parent_id';
  if (!childrenAttr) childrenAttr = 'children';

  var treeList = [];
  var lookup = {};
  list.forEach(function (obj) {

    lookup[obj[idAttr]] = obj;
    obj[childrenAttr] = [];
  });
  list.forEach(function (obj) {
    if (obj[parentAttr] != null) {

      lookup[obj[parentAttr]][childrenAttr].push(obj);
    } else {
      treeList.push(obj);
    }
    cleanListAttrs(obj) // TODO: WARNING, error prone
  });
  return treeList;
}

function cleanListAttrs(obj) {
  delete obj['idParent']
  delete obj['level']
}

function flatten_another(children, getChildren, level, parent) { //doesn't works for now
                                                                 // from https://stackoverflow.com/a/44826889
  Array.prototype.concat.apply(
      children.map(x => ({...x, level: level || 1, parent: parent || null})),
      children.map(x => flatten(getChildren(x) || [], getChildren, (level || 1) + 1, x.id))
  )
}

function flatten(treeObj, idAttr, parentAttr, childrenAttr, levelAttr) {
  console.log('before flatting', treeObj)

  if (!idAttr) idAttr = 'id';
  if (!parentAttr) parentAttr = 'parent_id';
  if (!childrenAttr) childrenAttr = 'children';
  if (!levelAttr) levelAttr = 'level';

  function flattenChild(childObj, parentId = null, level) {
    let array = [];

    let childCopy = clonedeep(childObj);
    childCopy[levelAttr] = level;
    childCopy[parentAttr] = parentId;
    delete childCopy[childrenAttr];
    array.push(childCopy);

    array = array.concat(processChildren(childObj, level));

    console.log('after flatting', array)

    return array;
  }

  function processChildren(obj, level) {
    if (!level) level = 0;
    let array = [];

    obj[childrenAttr].forEach(function (childObj) {
      array = array.concat(flattenChild(childObj, obj[idAttr], level + 1));
    });

    return array;
  };

  let result = processChildren({children: treeObj});
  return result;
}

// let tl = [
//   {
//     id: 0,
//     text: 'Andy',
//     children: [],
//     done: false
//   }, {
//     id: 1,
//     text: 'Harry',
//
//     children: [{
//       id: 2,
//       text: 'David',
//       done: false,
//       children: []
//     }],
//     done: false
//   }, {
//     id: 3,
//     text: 'Lisa',
//     children: [],
//     done: false
//   }
// ]

// let tasksJson = [
//       {
//         "id": 0,
//         "text": "Andy",
//         "children": [],
//         "done": false
//       }, {
//         "id": 1,
//         "text": "Harry",
//
//         "children": [{
//           "id": 2,
//           "text": "David",
//           "done": false
//         }],
//         "done": false
//       }, {
//         "id": 3,
//         "text": "Lisa",
//         "children": [],
//         "done": false
//       }
//     ]
//
// let tasks = JSON.parse(tasksJson)
// console.log(tasks)
// let json3 = `{"tasks": [
//       {
//         "id": 0,
//         "text": "Andy",
//         "children": [],
//         "done": false
//       }, {
//         "id": 1,
//         "text": "Harry",
//
//         "children": [{
//           "id": 2,
//           "text": "David",
//           "done": false,
//           "children": []
//         }],
//         "done": false
//       }, {
//         "id": 3,
//         "text": "Lisa",
//         "children": [],
//         "done": false
//       }
//     ]
//     }`
// let tasksObj = JSON.parse(json3).tasks
// console.log(tasksObj)
// console.log(flatten(tasksObj))
//
// //console.log(getNode(tl, 2))
// //console.log(flatten(tl, 'children', 'level', 'idParent'))
//
// //tl.forEach((value) => console.log(flatten_another(value, 'id', 'idParent', 'children')))
// console.log(tl)
// let flat = flatten(tl, 'id', 'idParent', 'children')
// console.log(flat)
// let treed = treeify(flat,'id','idParent','children')
// console.log(treed)
//
// //console.log(tl, flatten_another)
