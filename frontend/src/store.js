import Vue from 'vue'
import Vuex from 'vuex'
import {deleteNode, getNode, treeify, flatten, setNodeProps, getFormattedDateNow} from '@/functions'
import taskListService from '@/services/TaskListService'


Vue.use(Vuex)

class EmptyTask {
  constructor(state) {
    this.id = 0
    this.created = getFormattedDateNow()
    this.changed = getFormattedDateNow()
    this.title = 'New task'
    this.description = ''
    this.due = null
    this.completed = false
    this.priority = 1
    this.children = []
  }
}

export default new Vuex.Store({
  state: {
    taskList: []
  },
  mutations: {
    UPDATE_TASK_LIST(state, newTaskList) {
      state.taskList = newTaskList
    },
    UPDATE_TASK(state, {id, changesObj}) {
      let task = getNode(state.taskList, id)
      setNodeProps(task, changesObj)
    },
    ADD_TASK(state, task) {
      state.taskList.push(task)
    },
    DELETE_TASK(state, id) {
      deleteNode(state.taskList, id)
    }
  },
  getters: { // TODO: rewrite with getters all state get accessing
    taskList(state) {
      return state.taskList
    }

  },
  actions: {
    deleteTask({commit, state}, id) {
      taskListService.deleteTask(id)
          .then(commit('DELETE_TASK', id))
          .catch(error => console.error('Delete task error:', error))
    },
    addTask({commit, state}) {
      const newTask = new EmptyTask(state)
      taskListService.createTask(newTask)
          .then(response => { // TODO: make normal flat to obj func, or use existing
            const receivedNewTask = response.data
            receivedNewTask['children'] = []
            commit('ADD_TASK', receivedNewTask)
          })
          .catch(error => console.error('Empty task creating error:', error))

    },
    updateTask({commit, state, dispatch}, {id, changesObj}) {
      changesObj['changed'] = getFormattedDateNow()
      taskListService.updateTask(id, changesObj)
          .then(commit('UPDATE_TASK', {id, changesObj}))
          .catch(error => console.error('Update task error:', error))
    },
    dragTask({commit, state, dispatch}, newTaskList) {
      commit('UPDATE_TASK_LIST', newTaskList)
      dispatch('pushFullState')
    },
    initialLoad({commit}) {
      taskListService.getTaskList()
          .then(response => {
            const tasks_flat = response.data
            const tasksObj = treeify(tasks_flat)
            commit('UPDATE_TASK_LIST', tasksObj)
          })
          .catch(error => {
            console.error('Initial load error (tasks):', error)
          })
    },
    pushFullState({state}) {
      console.log('on push full', state.taskList)
      const flattenList = flatten(state.taskList)
      flattenList.forEach(item => { //TODO: yes... not in single request
            console.log('in for each', item)
            taskListService.updateTask(item.id, item)
                .then(resp => console.log(resp, 'cool'))
                .catch(error => {
                      console.error('Initial load error (tasks):', error)
                    }
                )
          }
      )
    }
  }
})
