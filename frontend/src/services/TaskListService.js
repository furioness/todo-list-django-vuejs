import axios from 'axios'


const apiClient = axios.create({
  baseURL: `http://127.0.0.1:8000/api`, // TODO: fix for release
  withCredentials: false, // This is the default
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  xsrfCookieName: 'csrftoken',
  xsrfHeaderName: 'X-CSRFToken',
})

export default {
  getTaskList() {
    return apiClient.get('/tasks/')
  },
  updateTask(id, flatTask){
    return apiClient.patch(`/task/${id}/`, flatTask)
  },
  deleteTask(id){
    return apiClient.delete(`/task/${id}/`)
  },
  createTask(task){
    return apiClient.post(`/tasks/`, task)
  },
  pushAllTasks(tasks){ //TODO: not working...
    return apiClient.patch(`/tasks/`, tasks)
  }
}
