from rest_framework import serializers

from todolist.models import Task, Project


class TaskSerializer(serializers.ModelSerializer):
    # queryset = Task.objects.all()

    class Meta:
        model = Task
        fields = '__all__'
        extra_kwargs = {
            'creator_id': {'read_only': True}
            # define the 'user' field as 'read-only'
        }

class ProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Project
        fields = '__all__'
        extra_kwargs = {
            'creator_id': {'read_only': True}
            # define the 'user' field as 'read-only'
        }
