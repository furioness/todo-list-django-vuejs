from django.contrib.auth.models import User
from django.http import request
from django.http import Http404

from rest_framework import generics
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import renderers
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.mixins import UpdateModelMixin
from rest_framework import status

from rest_framework.views import APIView

from todolist.models import Task, Project
from api.serializers import TaskSerializer, ProjectSerializer


class TaskList(APIView):
    """
    List all snippets, or create a new snippet.
    """

    def get(self, request, format=None):
        tasks = Task.objects.filter(creator_id=request.user.id)
        serializer = TaskSerializer(tasks, many=True)

        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = TaskSerializer(data=request.data)
        if serializer.is_valid():
            serializer.validated_data['creator_id'] = request.user
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # def patch(self, *args, **kwargs):
    #     super(TaskList, self).perform_update(*args, **kwargs)


class TaskDetail(APIView):
    def get_object(self, pk, user_id):
        try:
            return Task.objects.get(pk=pk, creator_id=user_id)
        except Task.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        task_obj = self.get_object(pk, request.user.id)
        serializer = TaskSerializer(task_obj)

        return Response(serializer.data)

    def patch(self, request, pk):
        task_obj = self.get_object(pk, request.user.id)
        serializer = TaskSerializer(task_obj, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        task_obj = self.get_object(pk, request.user.id)
        if task_obj:  # Delete all children too
            Task.objects.filter(parent_id=task_obj).delete()
            task_obj.delete()

            return Response(status=status.HTTP_202_ACCEPTED)
        return Response(status=status.HTTP_400_BAD_REQUEST)

class ProjectList(APIView):
    """
    List all snippets, or create a new snippet.
    """

    def get(self, request, format=None):
        projects = Project.objects.filter(creator_id=request.user.id)
        serializer = ProjectSerializer(projects, many=True)

        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ProjectSerializer(data=request.data)
        if serializer.is_valid():
            serializer.validated_data['creator_id'] = request.user
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProjectDetail(APIView):
    def get_object(self, pk, user_id):
        try:
            return Project.objects.get(pk=pk, creator_id=user_id)
        except Task.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        project_obj = self.get_object(pk, request.user.id)
        serializer = ProjectSerializer(project_obj)

        return Response(serializer.data)

    def patch(self, request, pk):
        project_obj = self.get_object(pk, request.user.id)
        serializer = ProjectSerializer(project_obj, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        project_obj = self.get_object(pk, request.user.id)
        if project_obj:
            project_obj.delete()
            return Response(status=status.HTTP_202_ACCEPTED)
        return Response(status=status.HTTP_400_BAD_REQUEST)
