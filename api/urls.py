from django.urls import path
from api.views import TaskList, TaskDetail, ProjectList, ProjectDetail

urlpatterns = [
    path(r'tasks/', TaskList.as_view()),
    path(r'task/<int:pk>/', TaskDetail.as_view()),
    path(r'projects/', ProjectList.as_view()),
    path(r'project/<int:pk>/', ProjectDetail.as_view()),
]
